# Dodod Template

Dieses Repository enthält das unglaublich tolle Übungsblatt-Template von Team dodod.

## Befehle
Das Template führt einige zusätzliche Befehle ein:

| *Befehl*                                         | *Erklärung*                                                                |
|--------------------------------------------------|----------------------------------------------------------------------------|
|`\blattnummer{123}`                               | Legt die Blattnummer für eine Abgabe fest.                                 |
|`\authors{a}{Max Mustermann}{123456}`  | Legt die Autoren eines Übungsblattes fest. Der erste Paramater ist ein Buchstabe, der bei jedem neuen Autor erhöht werden muss. Autor 2 bekommt also *b*, Autor 3 *c* usw.      |
|`\inputJava{/path/to/java/file}`                  | Fügt eine Java-Datei mit Syntax-Highlighting ein.                          |
|`\inputMips{/path/to/asm/file}`                   | Fügt ein Mips-Assembler-Programm mit Syntax-Highlighting ein.              | 
|`\begin{toshow}` <br>`your text`<br> `\end{toshow}`           | Für Beweise: "Zu zeigen" in schön formatiert                               |
|`\begin{lefttoshow}` <br> `your text` <br> ` \end{lefttoshow`}   | Für Beweise: "Noch zu zeigen" in schön formatiert                          |
