\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{uebungsblatt}[]
\LoadClass[ngerman]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{listing}
\usepackage[a4paper]{geometry}
\usepackage[ngerman]{babel}
\usepackage{amssymb} %Zeugs für Darstellung von Mathesymbolen (Bsp. N der nat. Zahlen)
\usepackage{ntheorem}
\usepackage{fancyhdr} %header + footer
\usepackage{lastpage} %Anzeige der Gesamtseitenzahl
\usepackage{mathtools} %Zeugs für Mathe-Formeln
\usepackage{enumitem}
\usepackage{commath}
\usepackage{stmaryrd} %Klammern
\usepackage[nameinlink]{cleveref}
\usepackage[]{tikzsymbols}
\usepackage{tikz}
\usepackage{suffix}
\usepackage{listings}
\usepackage{pgfplots}
\usepackage{booktabs}
\usepackage{dirtytalk}
\usepackage{etoolbox}
\usepackage{gauss}
\RequirePackage[framemethod=default]{mdframed}

%Replace * with cdot
\DeclareMathSymbol{*}{\mathbin}{symbols}{"01}


\newcommand*\colvec[3][]{
     \begin{pmatrix}\ifx\relax#1\relax\else#1\\\fi#2\\#3\end{pmatrix}
       }

% Mips Assembler Syntax Highlighting
\lstdefinelanguage[mips]{Assembler}{%
  % so listings can detect directives and register names
  alsoletter={.\$},
  % strings, characters, and comments
  morestring=[b]",
  morestring=[b]',
  morecomment=[l]\#,
  % instructions
  morekeywords={[1]abs,abs.d,abs.s,add,add.d,add.s,addi,addiu,addu,%
    and,andi,b,bc1f,bc1t,beq,beqz,bge,bgeu,bgez,bgezal,bgt,bgtu,%
    bgtz,ble,bleu,blez,blt,bltu,bltz,bltzal,bne,bnez,break,c.eq.d,%
    c.eq.s,c.le.d,c.le.s,c.lt.d,c.lt.s,ceil.w.d,ceil.w.s,clo,clz,%
    cvt.d.s,cvt.d.w,cvt.s.d,cvt.s.w,cvt.w.d,cvt.w.s,div,div.d,div.s,%
    divu,eret,floor.w.d,floor.w.s,j,jal,jalr,jr,l.d,l.s,la,lb,lbu,%
    ld,ldc1,lh,lhu,li,ll,lui,lw,lwc1,lwl,lwr,madd,maddu,mfc0,mfc1,%
    mfc1.d,mfhi,mflo,mov.d,mov.s,move,movf,movf.d,movf.s,movn,movn.d,%
    movn.s,movt,movt.d,movt.s,movz,movz.d,movz.s,msub,msubu,mtc0,mtc1,%
    mtc1.d,mthi,mtlo,mul,mul.d,mul.s,mulo,mulou,mult,multu,mulu,neg,%
    neg.d,neg.s,negu,nop,nor,not,or,ori,rem,remu,rol,ror,round.w.d,%
    round.w.s,s.d,s.s,sb,sc,sd,sdc1,seq,sge,sgeu,sgt,sgtu,sh,sle,%
    sleu,sll,sllv,slt,slti,sltiu,sltu,sne,sqrt.d,sqrt.s,sra,srav,srl,%
    srlv,sub,sub.d,sub.s,subi,subiu,subu,sw,swc1,swl,swr,syscall,teq,%
    teqi,tge,tgei,tgeiu,tgeu,tlt,tlti,tltiu,tltu,tne,tnei,trunc.w.d,%
    trunc.w.s,ulh,ulhu,ulw,ush,usw,xor,xori},
  % assembler directives
  morekeywords={[2].align,.ascii,.asciiz,.byte,.data,.double,.extern,%
    .float,.globl,.half,.kdata,.ktext,.set,.space,.text,.word},
  % register names
  morekeywords={[3]\$0,\$1,\$2,\$3,\$4,\$5,\$6,\$7,\$8,\$9,\$10,\$11,%
    \$12,\$13,\$14,\$15,\$16,\$17,\$18,\$19,\$20,\$21,\$22,\$23,\$24,%
    \$25,\$26,\$27,\$28,\$29,\$30,\$31,%
    \$zero,\$at,\$v0,\$v1,\$a0,\$a1,\$a2,\$a3,\$t0,\$t1,\$t2,\$t3,\$t4,
    \$t5,\$t6,\$t7,\$s0,\$s1,\$s2,\$s3,\$s4,\$s5,\$s6,\$s7,\$t8,\$t9,%
    \$k0,\$k1,\$gp,\$sp,\$fp,\$ra},
}[strings,comments,keywords]

\newcommand{\inputMips}[1]{\lstset{numbers=left,xleftmargin=2em,frame=single,framexleftmargin=2em,language=[MIPS]Assembler}\ifx&#1\else\lstinputlisting{#1}\fi}

% Java Syntax Highlighting
\definecolor{javared}{rgb}{0.6,0,0} % for strings
\definecolor{javagreen}{rgb}{0.25,0.5,0.35} % comments
\definecolor{javapurple}{rgb}{0.5,0,0.35} % keywords
\definecolor{javadocblue}{rgb}{0.25,0.35,0.75} % javadoc
\newcommand{\inputJava}[1]{\lstset{language=Java,
	basicstyle=\ttfamily,
	keywordstyle=\color{javapurple}\bfseries,
	stringstyle=\color{javared},
	commentstyle=\color{javagreen},
	breaklines=true,
	morecomment=[s][\color{javadocblue}]{/**}{*/},
	numbers=left,
	numberstyle=\tiny\color{black},
	stepnumber=1,
	numbersep=10pt,
	literate={Ö}{{\"O}}1 {Ä}{{\"A}}1 {Ü}{{\"U}}1 {ß}{{\ss}}1 {ü}{{\"u}}1 {ä}{{\"a}}1 {ö}{{\"o}}1,
	tabsize=4,
	showspaces=false,
	showstringspaces=false}\lstinputlisting{#1}}

\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\allowdisplaybreaks

\setlength{\headheight}{50pt}

\newcommand\authors[3]{\@namedef{author#1}{#2}\@namedef{matrikelnummer#1}{#3}}

\newcommand\blattnummer[1]{\renewcommand\@blattnummer{#1}}
\newcommand\@blattnummer{\@latex@error{No \noexpand\blattnummer given}\@ehc}

\newcommand\modul[1]{\renewcommand\@modul{#1}}
\newcommand\@modul{\@latex@error{No \noexpand\modul given}\@ehc}

\newcommand\gruppenbeschreibung[1]{\renewcommand\@gruppenbeschreibung{#1}}
\newcommand\@gruppenbeschreibung{\@latex@error{No \noexpand\gruppenbeschreibung given}\@ehc}

\newcounter{aufgabencounter}
\newcommand{\aufgabe}[1]{\stepcounter{aufgabencounter}\section*{Aufgabe \@blattnummer.\arabic{aufgabencounter}\ifx&#1&\else\ - #1\fi}}
\WithSuffix\newcommand\aufgabe*[1]{\section*{Aufgabe #1}}

\newcounter{teilaufgabencounter}[aufgabencounter]
\newcommand{\teilaufgabe}[1]{\stepcounter{teilaufgabencounter}\subsection*{Teilaufgabe \@blattnummer.\arabic{aufgabencounter}.\arabic{teilaufgabencounter}\ifx&#1&\else\ - #1\fi}}
\WithSuffix\newcommand\teilaufgabe*[1]{\subsection*{Teilaufgabe #1}}

\definecolor{secondary}{HTML}{84B819}
\definecolor{secondaryLight}{HTML}{E5FABB}
\definecolor{givenColor}{HTML}{333333}
\definecolor{givenColorLight}{HTML}{F0F0F0}

\newtheorem*{toshowT}{Zu zeigen}
\newtheorem*{lefttoshowT}{Noch zu zeigen}
\newtheorem*{givenT}{Gegeben}
\newmdenv[skipabove=7pt,
skipbelow=7pt,
rightline=false,
leftline=true,
topline=false,
bottomline=false,
backgroundcolor=secondaryLight,
linecolor=secondary,
innerleftmargin=5pt,
innerrightmargin=5pt,
innertopmargin=5pt,
innerbottommargin=5pt,
leftmargin=0cm,
rightmargin=0cm,
linewidth=4pt]{tsBox}

\newmdenv[skipabove=7pt,
skipbelow=7pt,
rightline=false,
leftline=true,
topline=false,
bottomline=false,
backgroundcolor=secondaryLight,
linecolor=secondary,
innerleftmargin=5pt,
innerrightmargin=5pt,
innertopmargin=5pt,
innerbottommargin=5pt,
leftmargin=0cm,
rightmargin=0cm,
linewidth=4pt]{ltsBox}

\newmdenv[skipabove=7pt,
skipbelow=7pt,
rightline=false,
leftline=true,
topline=false,
bottomline=false,
backgroundcolor=givenColorLight,
linecolor=givenColor,
innerleftmargin=5pt,
innerrightmargin=5pt,
innertopmargin=5pt,
innerbottommargin=5pt,
leftmargin=0cm,
rightmargin=0cm,
linewidth=4pt]{gBox}

\newenvironment{lefttoshow}{\begin{ltsBox}\begin{lefttoshowT}}{\end{lefttoshowT}\end{ltsBox}}
\newenvironment{toshow}{\begin{tsBox}\begin{toshowT}}{\end{toshowT}\end{tsBox}}
\newenvironment{given}{\begin{gBox}\begin{givenT}}{\end{givenT}\end{gBox}}

\AtBeginDocument{%
\pagestyle{fancy}
\fancyhead[L]{\@modul\\Übungsblatt \@blattnummer\\\today}
\fancyhead[C] {}
\fancyhead[R] {
\authora\ - Mat. Nr. \matrikelnummera\\
\ifdefined\authorb\authorb\ - Mat. Nr. \matrikelnummerb\\\fi
\ifdefined\authorc\authorc\ - Mat. Nr. \matrikelnummerc\\\fi
\ifdefined\authord\authord\ - Mat. Nr. \matrikelnummerd\\\fi
\ifdefined\authore\authore\ - Mat. Nr. \matrikelnummere\\\fi
\ifdefined\authorf\authorf\ - Mat. Nr. \matrikelnummerf\\\fi
\@gruppenbeschreibung
}

\fancyfoot[C] {}
\fancyfoot[R] {Seite \thepage \ von \pageref{LastPage}}
}
